from django.urls import path
from todos.views import list_of_topics, topic_details, create_topic, edit_topic, delete_topic, create_task, edit_task

urlpatterns = [
    path("", list_of_topics, name="todo_list_list"),
    path("<int:id>/", topic_details, name="todo_list_detail"),
    path("create/", create_topic, name="todo_list_create"),
    path("<int:id>/edit/", edit_topic, name="todo_list_update"),
    path("<int:id>/delete/", delete_topic, name="todo_list_delete"),
    path("items/create/", create_task, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_task, name="todo_item_update"),
]
