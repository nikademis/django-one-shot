from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TaskForm


# Create your views here.
def list_of_topics(request):
    topics = TodoList.objects.all()
    context = {
        "topic_list": topics,
    }
    return render(request, "todos/list.html", context)


def topic_details(request, id):
    topic = get_object_or_404(TodoList, id=id)
    context = {
        "topic": topic,
    }
    return render(request, "todos/detail.html", context)


def create_topic(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            topic = form.save()
            return redirect("todo_list_detail", id=topic.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            topic = form.save()
            return redirect("todo_list_detail", id=topic.list.id)
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def edit_topic(request, id):
    topic = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=topic)
        if form.is_valid():
            topic = form.save()
            return redirect("todo_list_detail", id=topic.id)
    else:
        form = TodoForm(instance=topic)
    context = {
        "topic": topic,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def edit_task(request, id):
    topic = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=topic)
        if form.is_valid():
            topic = form.save()
            return redirect("todo_list_detail", id=topic.list.id)
    else:
        form = TaskForm(instance=topic)
    context = {
        "topic": topic,
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)


def delete_topic(request, id):
    topic = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        topic.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")
